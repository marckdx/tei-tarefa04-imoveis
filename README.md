### Marco e Prince Imóveis... ###
Sistema que deverá efetuar os CRUD de Imóveis de uma Imobiliária.


Ver o sistema funcionando:
[Sistema de Cadastro de Imóveis](http://tei.marcolima.hol.es/tarefa-4)

Sponsor: Leonardo Villani

Requisitos:

* Inserir um imóvel;
* Editar um imóvel;
* Excluir um imóvel;
* Pesquisar por imóvel;
* Listar  imóveis;
* Mostrar detalhes do imóvel com os imóveis recentes.

Ferramentas e conhecimentos empregados

* Twitter Bootstrap;
* Sublime Text;
* MySQL;
* Hospedagem;
* PHP;

Qualquer dúvida ou sugestão enviar para contato@megari.com.br

Desenvolvido por [Michel Prince](http://mprince.zz.vc) e [Marco Aurélio Lima](http://www.marcolima.hol.es)